const express = require("express");
const cors = require("cors");
const got = require("got");

const app = express();
app.use(cors());

app.get("*", (req, res, _next) => {
  if(req.query.url) {
    got(req.query.url).then(function(response) {
      res.end(response.body)
    }).catch(function() {
      res.end()
    })
  } else {
    res.end()
  }
});

const PORT = 1234;
app.listen(PORT, () => console.log(`listening on port ${PORT}`));
